opendev/gerrit
==============

THIS PROJECT IS RETIRED.  PLEASE SEE THE UPSTREAM PROJECT AT

  https://gerrit.googlesource.com/gerrit/

This project was retired on 2022-03-16.  It was a mirror of the
upstream repository with patches for the review.opendev.org service
applied.

The opendev.org project has since moved to a model of building
directly from upstream source.

Archival notes
==============

This repository held the Gerrit source used by the OpenDev (nee
OpenStack) project before the migration to Gerrit 3.0.

The original branches have been removed from this project.  However,
for archival purposes, the following versions were run in production
with the only modifications to upstream as per the merged changes:

* 2.2.1
  https://review.opendev.org/q/project:opendev%252Fgerrit+status:merged+branch:openstack%252F2.2.1
* 2.3.0
  https://review.opendev.org/q/project:opendev%252Fgerrit+status:merged+branch:openstack%252F2.3.0
* 2.3.0-rc0
  https://review.opendev.org/q/project:opendev%252Fgerrit+status:merged+branch:openstack%252F2.3.0-rc0
* 2.4
  https://review.opendev.org/q/project:opendev%252Fgerrit+status:merged+branch:openstack%252F2.4
* 2.4.1
  https://review.opendev.org/q/project:opendev%252Fgerrit+status:merged+branch:openstack%252F2.4.1
* 2.4.2
  https://review.opendev.org/q/project:opendev%252Fgerrit+status:merged+branch:openstack%252F2.4.2
* 2.4.4
  https://review.opendev.org/q/project:opendev%252Fgerrit+status:merged+branch:openstack%252F2.4.4
* 2.8
  https://review.opendev.org/q/project:opendev%252Fgerrit+status:merged+branch:openstack%252F2.8
* 2.8.3
  https://review.opendev.org/q/project:opendev%252Fgerrit+status:merged+branch:openstack%252F2.8.3
* 2.8.4
  https://review.opendev.org/q/project:opendev%252Fgerrit+status:merged+branch:openstack%252F2.8.4
* 2.9.4
  https://review.opendev.org/q/project:opendev%252Fgerrit+status:merged+branch:openstack%252F2.9.4
* 2.10.2
  https://review.opendev.org/q/project:opendev%252Fgerrit+status:merged+branch:openstack%252F2.10.2
* 2.11.4
  https://review.opendev.org/q/project:opendev%252Fgerrit+status:merged+branch:openstack%252F2.11.4
* 2.11.9
  https://review.opendev.org/q/project:opendev%252Fgerrit+status:merged+branch:openstack%252F2.11.9
* 2.13
  https://review.opendev.org/q/project:opendev%252Fgerrit+status:merged+branch:openstack%252F2.13
* 2.13.3
  https://review.opendev.org/q/project:opendev%252Fgerrit+status:merged+branch:openstack%252F2.13.3
* 2.13.4
  https://review.opendev.org/q/project:opendev%252Fgerrit+status:merged+branch:openstack%252F2.13.4
* 2.13.5
  https://review.opendev.org/q/project:opendev%252Fgerrit+status:merged+branch:openstack%252F2.13.5
* 2.13.7
  https://review.opendev.org/q/project:opendev%252Fgerrit+status:merged+branch:openstack%252F2.13.7
* 2.13.8
  https://review.opendev.org/q/project:opendev%252Fgerrit+status:merged+branch:openstack%252F2.13.8
